package com.gitee.wenbo0.annotation;

/**
 * @author wenbo
 * @since 2022/08/15 10:12
 */
public enum ConditionType {
    AND,
    OR
}
