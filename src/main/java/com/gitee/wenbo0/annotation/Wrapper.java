package com.gitee.wenbo0.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wenbo
 * @since 2022/5/10 14:38
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Wrapper {
    /**
     * 条件类型
     */
    WrapperType value() default WrapperType.EQ;

    /**
     * 数据库字段名 默认为pojo字段名小驼峰转下划线
     */
    String columnName() default "";

    /**
     * 是否忽略空白字符串
     */
    boolean ignoreBlank() default true;

    /**
     * 是否忽略null
     */
    boolean ignoreNull() default true;

    /**
     * 条件组名称
     */
    String group() default "";
}
