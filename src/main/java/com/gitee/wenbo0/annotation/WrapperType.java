package com.gitee.wenbo0.annotation;

/**
 * @author wenbo
 * @since 2022/5/10 15:09
 */
public enum WrapperType {
    EQ,
    NE,
    LIKE,
    NOT_LIKE,
    LIKE_LEFT,
    LIKE_RIGHT,
    LT,
    GT,
    LE,
    GE,
    IN,
    NOT_IN
}
