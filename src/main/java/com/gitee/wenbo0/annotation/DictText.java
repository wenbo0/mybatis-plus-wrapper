package com.gitee.wenbo0.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wenbo
 * @since 2022/7/1 14:42
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DictText {
    /**
     * 要查询的关联表的条件字段名
     */
    String keyColumn() default "id";

    /**
     * 要查询的关联表的回显字段名
     */
    String textColumn() default "name";

    /**
     * 实体类中要回写的字段名
     */
    String target() default "";

    /**
     * 要查询的关联表名称
     */
    String tableName() default "";

    /**
     * 关联表其他需要的条件字段名
     */
    String otherColumn() default "dict_id";

    /**
     * 关联表其他需要的条件字段值
     */
    String otherValue() default "";

    /**
     * 是否为逗号分割 如果设置为true 回写的字典名称同样为逗号分割
     */
    boolean commaSeparate() default false;
}
