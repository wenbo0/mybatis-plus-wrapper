package com.gitee.wenbo0.annotation;

/**
 * @author wenbo
 * @since 2022/08/15 10:55
 */
public @interface Group {
    /**
     * 定义条件组名称
     */
    String group();

    /**
     * 条件组内条件拼接类型（括号内）
     */
    ConditionType conditionType() default ConditionType.AND;

    /**
     * 条件组外拼接类型（括号外）
     */
    ConditionType splicingType() default ConditionType.AND;
}
